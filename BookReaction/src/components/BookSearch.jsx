/**
 * @jsx React.DOM
 */

'use strict';

var React = require('react');
var BookGallery = require('./BookGallery.jsx');

var BookSearch = React.createClass({
  getInitialState() {
    return {
      query: '',
      books: [],
      checked: true,
      bookTitle: "" 
    };
  },
  search() {
    const BASE_URL = 'https://www.googleapis.com/books/v1/volumes?maxResults=40&q=';
    this.checked = !this.checked;
    if (this.query) {
      console.log("Do the search with " + this.query);

      fetch(BASE_URL + this.query, { method: 'GET' })
        .then(response => response.json())
        .then(json => this.setState({ books: json.items }));

     
    } else {
      console.log("Nothing to search for...");

    }
  },

  render() {
    return (
      
      <div className="Global">
        <h3>Book Search</h3>
        <p>
          <input
              type="search"
              id="inputBookTitle"
              onChange={event => this.query = event.target.value}
              onKeyUp={event => {
                if (event.key == "Enter") {
                  this.search();
                }
              }}
            />
          <span style={{ border: '1px solid garkgrey', padding : '5px', background: 'lightgray' }} >
            <span className="glyphicon glyphicon-search" onClick={() => this.search()} ></span>
          </span>
        </p> 
        <BookGallery list={this.state.books}/>
      </div>
    );
  }
});

module.exports = BookSearch;
