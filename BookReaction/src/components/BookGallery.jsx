/**
 * @jsx React.DOM
 */

'use strict';

var React = require('react');

var BookGallery = React.createClass({
  getInitialState() {
    return {
      books: []
    };
  },
  rate() {

    console.log("Rate the book");
  },
  render() {
    this.books = this.props.list || [];
    console.log("displaying " + this.books.length);
    return (
      <div id="bookContainer" >
        <h4>Results:</h4>
        <ul id="gallery">
          {
            this.books.map((book, index) => {
              let { title, imageLinks, infoLink, authors } = book.volumeInfo;
              return (
                <li key={index}>
                  <a href={infoLink} target="_blank">
                    <div className="book">
                      <img className="book-img" alt="cover" src={(imageLinks && imageLinks.thumbnail) ? "http://books.google.com/books/content?id=" + book.id + "&printsec=frontcover&img=1&zoom=1" : ''} />
                      <span className="book-title">{title}<br /><span className="book-authors">By {authors.toString()}</span></span>
                    </div>
                  </a>
                </li>
              )
            })
          }
        </ul>
      </div>
    );
  }
});

module.exports = BookGallery;
